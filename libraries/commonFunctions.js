//==================================================================
//Este código se encarga de obtener funciones comunes del sistema ==
//==================================================================
var axios = require("axios");
var mongoose = require('mongoose'); // Conector de la DB

// Función para obtener la fecha/hora actual en UTC
module.exports.currentDate = function() {
  var baseDate = new Date();
  var fecha = baseDate.getFullYear() + '-' + (baseDate.getMonth()+1) + '-' + baseDate.getDate() + ' ' + baseDate.getHours() + ':' + baseDate.getMinutes() + ':' + baseDate.getSeconds();
  return fecha.toString() + '.000Z';
}

// Función para obtener el índice de un array de objetos buscando por propiedad del objeto
module.exports.arrayObjectIndexOf = function(myArray, searchTerm, property) {
  for(var i = 0, len = myArray.length; i < len; i++) {
    if (myArray[i][property] == searchTerm) {
      return i;
    }
  }
  return -1;
}

// Función para pintar log fácil de verificar
module.exports.consoleOutput = function(content){
  console.log("******************************************************************");
  console.log(content);
  console.log("******************************************************************");
}

// Función para hacer un request y obtener un mensaje de exito/error
module.exports.axiosGetResult = function(method, url, req, res, callback){
  //Hace el redireccionamiento hacia la página inicial
  //this.consoleOutput(req.headers.referer);
  axios({
    method: method,
    url: process.env.DOMAIN + url,
    headers: {'x-access-token': req.token},
    data: req.body
  })
    .then(function (response) {
      if(response.data.success == true){
        //res.render(template, {success: true, message: response.data.message, token: req.token, user: req.decoded.user, result: response.data.data, redirect: req.headers.referer});
        //res.json({success: true, message: response.data.message, token: req.token, user: req.decoded.user, result: response.data.result, redirect: response.data.redirect});
        // res.json(response.data);
        callback(response.data);
      }
      else {
        callback({success: false, message: response.data.message, token: req.token, user: req.decoded.user})
        //res.render("web/message", {success: false, message: response.data.message, token: req.token, user: req.decoded.user, redirect: "/web?token=" + req.token });
      }
    })
    .catch(function (error) {
      callback({
        success: false,
        message: error,
        token: req.token,
        user: req.decoded.user,
        error: error,
      })
  })
}

module.exports.ucfirst = function(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports.dateWithoutTZ = function(date){
  return new Date(date.setSeconds(date.getSeconds()));
}

module.exports.dateUTCtoGMT = function() {
  var fecha = new Date();
  fecha.setHours(fecha.getHours() + Number(process.env.GMT));
  return fecha;
}

module.exports.formatoFecha = function (oldDate) {
  var month = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  var days = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
  return days[oldDate.getDay()] + ", " + month[oldDate.getMonth()]  + " " + agregarCeros(oldDate.getDate()) +" de " + oldDate.getFullYear() + " @" + agregarCeros(oldDate.getHours()) + ':' + agregarCeros(oldDate.getMinutes());
}

function agregarCeros(valor){
  if(valor < 10){
    valor = '0'+valor
  }
  return valor;
}

// Función para convertir los campos enviados desde un formulario al tipo de campo esperado por la DB
// La función recibe el modelo y el objeto que desea validar, devuelve el mismo objeto transformado.
module.exports.convertModelType = function(model, object){
  schema = model.schema.paths;
  // Recorre todas las propiedades el esquema
  for (property in schema) {
    var field = schema[property].path;
    // Valida que el usuario esté enviando un campo del esquema
    if(typeof(object[field]) !== 'undefined'){
      // Valida si es un ID
      if(schema[property].instance == 'ObjectID'){
        // Valida si viene un objeto especial o la cadena con el ID
        if (typeof object[field] !== 'object') {
          object[field] = mongoose.Types.ObjectId(object[field]);
        }
      }
      // Valida si es booleano
      /*
      if(schema[property].instance == 'Boolean'){
        object[field] = (object[field] == 'true');
      }
      */
      // Valida si es numero
      if(schema[property].instance == 'Number'){
        object[field] = +object[field];
      }
    }
  }
  return object;
}