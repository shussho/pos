//=================================================================
//Este código se encarga de hacer las peticiones básicas a la DB ==
//=================================================================

var mongoose = require("mongoose"); // Conector de la DB

exports.createDocument = function (Handler, callback) {
  Handler.save(function (err, result) {
    if (err) {
      callback({ success: false, message: err });
    } else {
      callback({
        success: true,
        message: "Guardamos los datos exitosamente",
        data: Handler,
      });
    }
  });
};

exports.findOne = function (Handler, conditions, callback) {
  Handler.findOne(conditions, function (err, result) {
    if (err) {
      callback({ success: false, message: err.message });
    } else {
      if (result) {
        callback({ success: true, data: result });
      } else {
        callback({ success: false });
      }
    }
  });
};

exports.findOneAndUpdate = function (Handler, req, callback) {
  Handler.findOneAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err.message });
      } else {
        if (result == null) {
          callback({ success: false, message: "El registro no existe" });
        } else {
          callback({
            success: true,
            message: "Datos actualizados exitosamente!",
            data: result,
          });
        }
      }
    }
  );
};

exports.deleteDocument = function (Handler, req, callback) {
  Handler.findOneAndRemove({ _id: req.params.id }, function (err, result) {
    if (err) {
      callback({ success: false, message: err });
    } else {
      if (result == null) {
        callback({ success: false, message: "El registro no existe!" });
      } else {
        callback({
          success: true,
          message: "El registro fue eliminado exitosamente!",
        });
      }
    }
  });
};

exports.listDocumentConditional = function (Handler, query, callback) {
  Handler.aggregate([query]).exec(function (err, result) {
    if (err) {
      callback({ success: false, message: err });
    } else {
      if (result.length == 0) {
        callback({
          success: false,
          message: "No hay resultados para la búsqueda",
          data: [],
        });
      } else {
        callback({ success: true, data: result });
      }
    }
  });
};

//*********************************************************************************************** */

exports.listDocuments = function (Handler, callback) {
  Handler.find({}, function (err, result) {
    if (err) {
      callback({ success: false, message: err });
    } else {
      if (result == "") {
        callback({ success: false, message: "No hay resultados" });
      } else {
        callback({ success: true, message: "", result: result });
      }
    }
  });
};

exports.findDocument = function (Handler, id, callback) {
  Handler.aggregate(
    [
      {
        $match: {
          _id: mongoose.Types.ObjectId(id),
        },
      },
    ],
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        callback({ success: true, result: result });
      }
    }
  );
};

exports.updateArrayElement = function (Handler, searchFields, set, callback) {
  Handler.updateOne(
    searchFields,
    {
      $set: set,
    },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result.n == 0) {
          callback({
            success: false,
            message: "El registro a editar no existe",
          });
        } else {
          if (result.nModified == 0) {
            callback({
              success: false,
              message: "Esta acción ya se había realizado",
            });
          } else {
            callback({
              success: true,
              message: "Registro actualizado exitosamente!",
              result: result,
            });
          }
        }
      }
    }
  );
};

exports.addToArray = function (Handler, searchFields, addToSet, callback) {
  mongoose.set("useFindAndModify", false);
  Handler.findOneAndUpdate(
    searchFields,
    {
      $addToSet: addToSet,
    },
    { new: true },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result.n == 0) {
          callback({
            success: false,
            message: "El registro a editar no existe",
          });
        } else {
          if (result.nModified == 0) {
            callback({
              success: false,
              message: "El registro ya existe en el arreglo",
            });
          } else {
            callback({
              success: true,
              message: "Registro actualizado exitosamente!",
              result: result,
            });
          }
        }
      }
    }
  );
};

exports.addToArrayMulti = function (Handler, searchFields, addToSet, callback) {
  mongoose.set("useFindAndModify", false);
  Handler.update(
    searchFields,
    {
      $addToSet: addToSet,
    },
    { new: true, multi: true },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result.n == 0) {
          callback({
            success: false,
            message: "El registro a editar no existe",
          });
        } else {
          if (result.nModified == 0) {
            callback({
              success: false,
              message: "El registro ya existe en el arreglo",
            });
          } else {
            callback({
              success: true,
              message: "Registro actualizado exitosamente!",
              result: result,
            });
          }
        }
      }
    }
  );
};

exports.removeFromArray = function (Handler, searchFields, pull, callback) {
  Handler.update(
    searchFields,
    {
      $pull: pull,
    },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result.n == 0) {
          callback({
            success: false,
            message: "El registro a editar no existe",
          });
        } else {
          if (result.nModified == 0) {
            callback({
              success: false,
              message: "El registro no fue modificado",
            });
          } else {
            callback({
              success: true,
              message: "Registro eliminado exitosamente!",
              result: result,
            });
          }
        }
      }
    }
  );
};

exports.updateDocument = function (Handler, req, callback) {
  Handler.findOneAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result == null) {
          callback({ success: false, message: "El registro no existe" });
        } else {
          callback({
            success: true,
            message: "Registro actualizado exitosamente!",
            result: result,
          });
        }
      }
    }
  );
};

exports.removeDocument = function (Handler, match, callback) {
  Handler.findOneAndRemove(match, function (err, result) {
    if (err) {
      callback({ success: false, message: err });
    } else {
      if (result == null) {
        callback({ success: false, message: "El registro no existe!" });
      } else {
        callback({
          success: true,
          message: "El registro fue eliminado exitosamente!",
        });
      }
    }
  });
};

exports.increment = function (Handler, searchFields, values, callback) {
  Handler.findOneAndUpdate(
    searchFields,
    {
      $inc: values,
    },
    { new: true, multi: true },
    function (err, result) {
      if (err) {
        callback({ success: false, message: err });
      } else {
        if (result.n == 0) {
          callback({
            success: false,
            message: "El registro a editar no existe",
          });
        } else {
          if (result.nModified == 0) {
            callback({
              success: false,
              message: "El registro ya existe en el arreglo",
            });
          } else {
            callback({
              success: true,
              message: "Registro actualizado exitosamente!",
              result: result,
            });
          }
        }
      }
    }
  );
};
