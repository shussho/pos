// =======================================
// Invocamos los paquetes requeridos =====
// =======================================
var express        = require('express');
//var boolParser     = require('express-query-boolean');
var app            = express();
require('dotenv').config(); // Carga la configuración inicial de la app a través del archivo .env
/*
var bodyParser     = require('body-parser');
var morgan         = require('morgan');
var mongoose       = require('mongoose');
var passport       = require('passport');
var methodOverride = require("method-override");
var rfs            = require('rotating-file-stream')

var appMiddleware  = require("./middleware/appMiddleware");
var apiMiddleware  = require("./middleware/apiMiddleware");
var routes         = require("./routes/defaultRoutes");
var appRoutes      = require("./routes/appRoutes");
var apiRoutes      = require("./routes/apiRoutes");
var authRoutes     = require("./routes/authRoutes");
var webRoutes      = require("./routes/webRoutes");

// =======================
// Configuración =========
// =======================
/*


// Motor de vistas para renderizar contenido
app.set('view engine', 'pug');

// Cofigura ruta estática para contenido estático
app.use("/public",express.static("public"));

// Permite el envío de PUT y DELETE
app.use(methodOverride("_method"));

// Asigna las variables de entorno a la APP, para accesarlas desde las vistas
app.locals.env = process.env;

// Puerto para el servidor web
var port = process.env.PORT

// Constantes para la conexión con la DB
const dbOptions = {
  user: process.env.DATABASE_USER,
  pass: process.env.DATABASE_PASS,
  useNewUrlParser: true
};

// Conexión con la DB
mongoose.connect('mongodb://' + process.env.DATABASE_HOST +':' + process.env.DATABASE_PORT + '/' + process.env.DATABASE_NAME, dbOptions);

// Usamos el body parser para obtener información GET/POST y/o parámetros URL
//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());
app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
app.use(boolParser());

// Usamos morgan para generar log de consultas a la consola
var fs = require('fs');
//var accessLogStream = fs.createWriteStream('./logs/access.log', { flags: 'a' })
//app.use(morgan('dev', { stream: accessLogStream }))

app.use(morgan('dev'));


// Usamos las reglas del middleware para la app
app.use("/app",appMiddleware);
app.use("/api",apiMiddleware);

// =======================
// Default Routes ========
// =======================

// Obtiene una instancia para las rutas de inicio
app.use("/",routes);


app.use(passport.initialize());

app.use("/auth",authRoutes);


// =======================
// APP Routes ============
// =======================

// Obtiene una instancia para las rutas del APP
app.use("/app",appRoutes);
app.use("/api",apiRoutes);


// =======================
// WEB Routes ============
// =======================

// Obtiene una instancia para las rutas del APP
app.use("/web",webRoutes);



// Maneja los errores 404
app.use(function(req, res, next){
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  res.status(404).json({
    success: false,
    message: 'La página que buscas no existe!',
    token: token
  });
});

*/

const {spawn} = require('child_process');

app.get('/', function(req, res) {
  //res.render("web/index");
  //res.send("Hola mundo");
  var dataToSend;
  // spawn new child process to call the python script
  const python = spawn('python', ['serial.py']);
  // collect data from script
  python.stdout.on('data', function (data) {
    console.log('Pipe data from python script ...');
    dataToSend = data.toString();
  });
  // in close event we are sure that stream from child process is closed
  python.on('close', (code) => {
    console.log(`child process close all stdio with code ${code}`);
    // send data to browser
    res.send("Peso del articulo: ..." + dataToSend + "...")
  });
});

// =======================
// Inicia el servidor ====
// =======================
var port = 8000;
app.listen(port);
console.log('Servidor activo en ' + process.env.DOMAIN);
