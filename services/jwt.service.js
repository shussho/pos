const jwt = require("jsonwebtoken"); // Crea, firma y verifica los tokens

class JwtService {
  constructor() {}

  // Crea payload para la creacion del token
  userData(user) {
    const tokenInfo = {
      _id: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      status: user.status,
    };
    return tokenInfo;
  }

  // Crea el token de sesion de usuario
  createToken(user) {
    const payload = {
      user: user,
    };
    const token = jwt.sign(payload, process.env.SECRET, {
      //expiresIn: '20000' // expira en 20 segundos
      //expiresIn: '24h' // expira en 24 horas
      expiresIn: process.env.TOKEN_EXPIRATION, // Si se omite, el token no expira
    });
    return token;
  }

  // Crea el token temporal para recuperar contraseña
  temporaryToken(user) {
    const payload = {
      user: user,
    };
    const token = jwt.sign(payload, process.env.TEMPORARY_SECRET, {
      expiresIn: process.env.TEMPORARY_TOKEN_EXPIRATION,
    });
    return token;
  }

  verify(token, secret, callback) {
    jwt.verify(token, secret, callback);
  }

  decode(token) {
    return jwt.decode(token);
  }
}

module.exports = JwtService;
