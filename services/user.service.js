const bcrypt = require("bcrypt");
const fs = require("fs");
const JwtService = require("./jwt.service");
const User = require("../models/users"); // Modelo de usuario
const apiREST_Handler = require("../libraries/apiREST");
const commonFunctions = require("../libraries/commonFunctions"); // Funciones comunes del sistema

const serviceJwt = new JwtService();

class UserService {
  constructor() {}

  // Función para crear el usuario
  create(req, callback) {
    if (typeof req.body.password === "undefined") {
      callback({
        success: false,
        message: "El usuario debe definir el password",
      });
    } else {
      this.validatePassword(
        req.body.password,
        req.body.passwordConfirmation,
        function (output) {
          if (!output.success) {
            callback(output);
          } else {
            let newUser = new User(req.body);
            newUser.status = true;
            newUser.registrationDate = commonFunctions.dateUTCtoGMT();

            const conditions = {
              $or: [
                { email: req.body.email },
                { cellphone: req.body.cellphone },
              ],
            };

            // Verifica que el usuario no exista en la DB
            apiREST_Handler.findOne(User, conditions, function (result) {
              if (result.success) {
                result.success = false;
                delete result.data;
                if (!result.message) {
                  result.message =
                    "El usuario ya está registrado, si no recuerda el password, por favor trate de recuperarlo";
                }
                callback(result);
              } else {
                // TODO:
                // Se envía notificación por correo con los datos
                //emailNotification(req.body.loginEmail,'Su usuario ha sido creado','<b>Este contenido es de prueba</b>');
                apiREST_Handler.createDocument(newUser, function (result) {
                  if (result.success) {
                    result.token = serviceJwt.createToken(result.data);
                  }
                  callback(result);
                });
              }
            });
          }
        }
      );
    }
  }

  // Función para actualizar usuario
  update(req, callback) {
    this.validatePassword(
      req.body.password,
      req.body.passwordConfirmation,
      function (output) {
        if (!output.success) {
          callback(output);
        } else {
          // Si existe el password, lo encripta
          if (typeof req.body.password !== "undefined") {
            req.body.password = bcrypt.hashSync(
              req.body.password,
              parseInt(process.env.SALT_ROUNDS)
            );
          }
          apiREST_Handler.findOneAndUpdate(User, req, function (result) {
            //TODO: Actualizar el token después de cambiar datos de usuario
            result.token = serviceJwt.createToken(result.data);
            callback(result);
          });
        }
      }
    );
  }

  // Función para borrar usuarios
  delete(req, callback) {
    apiREST_Handler.deleteDocument(User, req, callback);
  }

  // Función para listar o buscar usuarios
  find(match, callback) {
    const query = [{ $match: match }];
    apiREST_Handler.listDocumentConditional(User, query, (result) => {
      // Elimina el password del objeto de retorno
      result.data.forEach((element) => {
        delete element.password;
      });
      callback(result);
    });
  }

  // Función para loguear al usuario
  login(req, callback) {
    const query = [
      {
        $match: {
          email: req.body.email.trim(),
        },
      },
    ];
    apiREST_Handler.listDocumentConditional(User, query, function (output) {
      if (output.data.length == 0) {
        callback({
          success: false,
          message: "Autenticación fallida, usuario no encontrado.",
        });
      } else {
        if (
          !bcrypt.compareSync(req.body.password.trim(), output.data[0].password)
        ) {
          callback({
            success: false,
            message: "La combinación de usuario y password es incorrecta",
          });
        } else {
          if (output.data[0].status == false) {
            callback({
              success: false,
              message:
                "El usuario se encuentra desactivado, no puede iniciar sesión",
            });
          } else {
            const token = serviceJwt.createToken(
              serviceJwt.userData(output.data[0])
            );
            req.decoded = output.data[0];
            delete output.data[0].password;
            callback({
              success: true,
              message: "Has iniciado sesión correctamente",
              data: output.data[0],
              token: token,
            });
          }
        }
      }
    });
  }

  // Función para validar el password de usuario
  validatePassword(password, passwordConfirmation, callback) {
    callback({ success: true, message: "Los passwords están bien" });
  }

  // Función para actualizar la foto de perfil
  updatePicture(req, callback) {
    const base64Image = req.body.imageData.split(";base64,").pop();
    fs.writeFile(
      "public/images/avatars/" + req.params.id,
      base64Image,
      { encoding: "base64" },
      function (err) {
        if (err) {
          callback({
            success: false,
            message: err,
            redirect: "/web/user/" + req.params.id + "?token=" + req.token,
            token: req.token,
            user: req.decoded.user,
          });
        } else {
          callback({
            success: true,
            message: "Imagen actualizada exitosamente!",
            redirect: "/web/user/" + req.params.id + "?token=" + req.token,
          });
        }
      }
    );
  }
}

module.exports = UserService;
