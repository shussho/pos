const Supplier = require("../models/suppliers"); // Modelo de proveedores
const apiREST_Handler = require("../libraries/apiREST");
const commonFunctions = require("../libraries/commonFunctions"); // Funciones comunes del sistema

class SupplierService {
  constructor() {}

  create(req, callback) {
    const new_Supplier = new Supplier(req.body);
    new_Supplier.supplierRegistrationDate = new Date();
    new_Supplier.supplierRegisteredById = req.decoded.user._id;
    new_Supplier.supplierStatus = true;
    apiREST_Handler.createDocument(new_Supplier, callback);
  }

  find(match, callback) {
    const query = [
      { $match: commonFunctions.convertModelType(Supplier, match) },
      { $sort: { optionOrder: 1 } },
    ];
    apiREST_Handler.listDocumentConditional(Supplier, query, callback);
  }

  update(req, callback) {
    apiREST_Handler.updateDocument(Supplier, req, callback);
  }

  delete(req, callback) {
    apiREST_Handler.deleteDocument(Supplier, req, callback);
  }
}

module.exports = SupplierService;
