const Item = require("../models/items");
const apiREST_Handler = require("../libraries/apiREST");
const commonFunctions = require("../libraries/commonFunctions"); // Funciones comunes del sistema

class ItemService {
  constructor() {}

  create(req, callback) {
    req.body.itemDate = new Date();
    const new_Item = new Item(req.body);
    apiREST_Handler.createDocument(new_Item, callback);
  }

  find(match, callback) {
    let query = [
      { $match: commonFunctions.convertModelType(Item, match) },
      { $sort: { optionOrder: 1 } },
    ];
    apiREST_Handler.listDocumentConditional(Item, query, callback);
  }

  update(req, callback) {
    apiREST_Handler.updateDocument(Item, req, callback);
  }

  delete(req, callback) {
    apiREST_Handler.deleteDocument(Item, req, callback);
  }
}

module.exports = ItemService;
