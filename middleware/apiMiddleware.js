const express = require("express");
const UserService = require("./../services/user.service");
const JwtService = require("../services/jwt.service");
const mongoose = require("mongoose"); // Conector de la DB

const jwtService = new JwtService();
const userService = new UserService();

const apiMiddleware = express();
// Enrutamiento del middleware para verificar el token
apiMiddleware.use(function (req, res, next) {
  // Cambia la salida agregando el token
  let oldSend = res.json;
  res.json = function (data) {
    // if(!data.success){
    //   data.message = "An error has occurred" // do something with the data
    // }
    if (typeof data.token == "undefined") {
      data.token = req.token;
    }
    res.json = oldSend; // set function back to avoid the 'double-send'
    return res.json(data); // just call as normal with data
  };

  // Se valida si la URL requiere token o no
  if (
    req.originalUrl == "/api/users/login" ||
    (req.originalUrl == "/api/users" && req.originalMethod == "POST")
  ) {
    // El usuario se está logueando o registrándose y no necesita token
    next();
  } else {
    // Verifica si el token viene en la cabecera, parametros GET o POST
    const token =
      req.body.token || req.query.token || req.headers["x-access-token"];
    if (token) {
      jwtService.verify(token, process.env.SECRET, function (err, decoded) {
        if (err) {
          //Verifica si el token expiró
          if (err.name == "TokenExpiredError" && 2 == 2) {
            //TODO: Validar si se debe resetear el token
            decoded = jwtService.decode(token);
            //Trae los datos del usuario actualizados

            match = {
              _id: mongoose.Types.ObjectId(decoded.user._id),
              email: decoded.user.email,
            };
            userService.find(match, function (result) {
              const newToken = jwtService.createToken(result.data[0]);
              console.log({
                success: true,
                message: "Your token has been renewed",
                data: result.data[0],
                token: newToken,
              });
              next();
            });
          } else {
            res.json({
              success: false,
              message: err.name + " - " + err.message,
            });
          }
        } else {
          //console.log(decoded);
          req.decoded = decoded;
          req.token = token;
          next();
        }
      });
    } else {
      // Si no existe el token, retorna error
      return res.status(403).send({
        success: false,
        message: "No existe token.",
      });
    }
  }
});

module.exports = apiMiddleware;
