const express = require("express");
const UserService = require("./../services/user.service");
const JwtService = require("../services/jwt.service");
const mongoose = require("mongoose"); // Conector de la DB
const webMiddleware = express();

const jwtService = new JwtService();
const userService = new UserService();

// Enrutamiento del middleware para verificar el token
webMiddleware.use(function (req, res, next) {
  // Verifica si el token viene en la cabecera, parametros GET o POST
  let token =
    req.body.token || req.query.token || req.headers["x-access-token"];
  if (token) {
    jwtService.verify(token, process.env.SECRET, (err, decoded) => {
      if (err) {
        //Verifica si el token expiró
        if (err.name == "TokenExpiredError" && 2 == 2) {
          //TODO: Validar si se debe resetear el token
          decoded = jwtService.decode(token);
          //Trae los datos del usuario actualizados
          match = {
            _id: mongoose.Types.ObjectId(decoded.user._id),
            email: decoded.user.email,
          };
          userService.find(match, function (result) {
            if (!result.data[0].status) {
              res.render("message", {
                success: false,
                message:
                  "Tu usuario ha sido desactivado y no puedes iniciar sesión",
                token: token,
                redirect: "/",
              });
            } else {
              const newToken = jwtService.createToken(result.data[0]);
              const newDecoded = jwtService.decode(newToken);
              req.decoded = newDecoded;
              req.token = newToken;
              req.query.token = newToken;
              console.log({
                success: true,
                message: "Se ha renovado su token",
                data: result.data[0],
                token: newToken,
              });
              next();
            }
          });
        } else {
          res.json({ success: false, message: err.name + " - " + err.message });
        }
      } else {
        req.decoded = decoded;
        req.token = token;
        next();
      }
    });
  } else {
    // Si no existe el token, retorna error
    return res.status(403).send({
      success: false,
      message: "No existe token.",
    });
  }
});

module.exports = webMiddleware;
