// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require("bcrypt");

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "Ingresa un email válido"];

// set up a mongoose model and pass it using module.exports
var user_schema = new Schema({
  email: {
    type: String,
    required: "El correo es obligatorio",
    trim: true,
    match: email_match
  },
  password: {
    type: String,
    required: true,
    trim: true
  },
  firstName: {
    type: String,
    required: "El nombre del usuario es obligatorio",
    trim: true
  },
  lastName: {
    type: String,
    required: "El apellido del usuario es obligatorio",
    trim: true
  },
  documentId: {
    type: String,
    required: "El documento de identidad es obligatorio",
    trim: true
  },
  phone: {
    type: String,
    trim:true
  },
  cellphone: {
    type: String,
    required: "El numero celular es obligatorio",
    trim: true
  },
  status: {
    type: Boolean,
    required: "El estado es obligatorio"
  },
  registrationDate : {
    type: Date,
    required: true
  }
}, { collection: 'users' });

// Valida el tamaño de la contraseña y la encripta para guardarla en la DB
user_schema.path('password').validate(function(password,){
  if(this.password.length < 8){
    return false;
  }
  this.password = bcrypt.hashSync(password, parseInt(process.env.SALT_ROUNDS));
  return true;
}, 'La contraseña es muy corta');


module.exports = mongoose.model("User",user_schema);

