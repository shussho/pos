// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


// set up a mongoose model and pass it using module.exports
var supplier_schema = new Schema({
  supplierType: {
    type: String,
    required: "El tipo de proveedor es obligatorio",
    trim: true,
  },
  supplierIdentification: {
    type: String,
    required: "La identificación del proveedor es obligatoria",
    trim: true,
  },
  supplierName: {
    type: String,
    required: "El nombre del proveedor es obligatorio",
    trim: true,
  },
  supplierSalesman: {
    type: String,
    trim: true
  },
  supplierMail: {
    type: String,
    required: "El email del proveedor es obligatorio",
    trim: true,
  },
  supplierPhone: {
    type: String,
    required: "El teléfono del proveedor es obligatorio",
    trim: true,
  },
  supplierAddress: {
    type: String,
    //required: "La direccion del proveedor es obligatoria",
    trim: true,
  },
  supplierCity: {
    type: String,
    //required: "La direccion del proveedor es obligatoria",
    trim: true,
  },
  supplierDescription: {
    type: String,
    trim: true,
  },
  supplierRegistrationDate : {
    type: Date,
    required: true
  },
  supplierRegisteredById: {
    type: Schema.Types.ObjectId
  },
  supplierStatus: {
    type: Boolean,
    required: "El estado es obligatorio"
  }
}, { collection: 'suppliers' });

module.exports = mongoose.model("Supplier",supplier_schema);

