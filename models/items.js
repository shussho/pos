// get an instance of mongoose and mongoose.Schema
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
var item_schema = new Schema(
  {
    supplier_id: {
      type: Schema.Types.ObjectId,
      required: "El id del proveedor del item es requerido",
    },
    itemName: {
      type: String,
      required: "El nombre del item es obligatorio",
      trim: true,
    },
    itemDate: {
      type: Date,
      required: true,
    },
    itemStatus: {
      type: Boolean,
      required: "El estado es obligatorio",
    },
  },
  { collection: "items" }
);

module.exports = mongoose.model("Item", item_schema);
