// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


// set up a mongoose model and pass it using module.exports
var profile_schema = new Schema({
  profileName: {
    type: String,
    required: "El nombre del perfil es obligatorio",
    trim: true,
  },
  profileLinks: [{
    urlName: String,
    urlLink: String,
    urlImage: String,
    urlStatus: Boolean
  }],
  profileStatus: {
    type: String,
    required: "El estado es obligatorio"
  }
}, { collection: 'profiles' });

module.exports = mongoose.model("Profile",profile_schema);

