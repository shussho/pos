var Profile = require("../models/profiles"); // Modelo de perfiles
var apiREST_Handler = require("../libraries/apiREST");
const { response } = require("express");

exports.create = function (req, callback) {
  var new_Profile = new Profile(req.body);
  apiREST_Handler.createDocument(new_Profile, callback);
};

exports.profile_ListAll = function (req, callback) {
  apiREST_Handler.listDocuments(Profile, callback);
};

exports.profile_Find = function (req, callback) {
  apiREST_Handler.findDocument(Profile, req.params.id, callback);
};

exports.profile_Update = function (req, callback) {
  apiREST_Handler.updateDocument(Profile, req, callback);
};

exports.profile_Delete = function (req, callback) {
  apiREST_Handler.deleteDocument(Profile, req, callback);
};
