// =======================================
// Invocamos los paquetes requeridos =====
// =======================================
const express = require("express");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const boolParser = require("express-query-boolean");
const mongoose = require("mongoose"); // Conector de la DB
const morgan = require("morgan");

require("dotenv").config(); // Carga la configuración inicial de la app a través del archivo .env

const app = express();

// =======================
// Configuración =========
// =======================

// Motor de vistas para renderizar contenido
app.set("view engine", "pug");
const path = require("path");
app.set("views", path.join(__dirname, "views"));

// Cofigura ruta estática para contenido estático
app.use("/public", express.static("public"));

// Permite el envío de PUT y DELETE
app.use(methodOverride("_method"));

// Asigna las variables de entorno a la APP, para accesarlas desde las vistas
app.locals.env = process.env;

// Puerto para el servidor web
const port = process.env.PORT;

// Constantes para la conexión con la DB
const dbOptions = {
  user: process.env.DATABASE_USER,
  pass: process.env.DATABASE_PASS,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

// Conexión con la DB
// try {
//   mongoose.connect(
//     "mongodb://" +
//       process.env.DATABASE_HOST +
//       ":" +
//       process.env.DATABASE_PORT +
//       "/" +
//       process.env.DATABASE_NAME,
//     dbOptions,
//     () => console.log("DB connected")
//   );
// } catch (error) {
//   console.log("DB could not connect");
//   console.log(error);
// }

// Agregar mongodb+srv para cluster en produccionn
const uri = `mongodb://${process.env.DATABASE_USER}:${process.env.DATABASE_PASS}@${process.env.DATABASE_HOST}/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`;

try {
  mongoose.connect(
    uri,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log("DB connected")
  );
  // mongoose.set("useFindAndModify", false); // Elimina warning
  // mongoose.set("useCreateIndex", true);
} catch (error) {
  console.log("DB could not connect");
  console.log(error);
}

// Usamos el body parser para obtener información GET/POST y/o parámetros URL
//app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.json());
app.use(bodyParser.json({ limit: "10mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));
app.use(boolParser());

app.use(morgan("dev"));

// =======================
// APP Routes ============
// =======================
const routerApi = require("./routes/apiRoutes");
routerApi(app);

// const routerDefault = require("./routes/defaultRoutes");
// // routerDefault(app);

const routerWeb = require("./routes/webRoutes");
routerWeb(app);

/*
// PROCESO DE PYTHON
const {spawn} = require('child_process');

app.get('/', function(req, res) {
  //res.render("web/index");
  //res.send("Hola mundo");
  var dataToSend;
  // spawn new child process to call the python script
  const python = spawn('python', ['serial.py']);
  // collect data from script
  python.stdout.on('data', function (data) {
    console.log('Pipe data from python script ...');
    dataToSend = data.toString();
  });
  // in close event we are sure that stream from child process is closed
  python.on('close', (code) => {
    console.log(`child process close all stdio with code ${code}`);
    // send data to browser
    res.send("Peso del articulo: ..." + dataToSend + "...")
  });
});
*/
// =======================
// Inicia el servidor ====
// =======================
app.listen(port);
console.log("Servidor activo en " + process.env.DOMAIN);
