let SWEET_MAIN_COLOR_BUTTON = "#0069d9";
const SWEET_SECONDARY_COLOR_BUTTON = "#f8f9fa";

function sweetMessage(icon, text) {
  if (icon == "") {
    icon = "info";
  }
  Swal.fire({
    icon: icon,
    confirmButtonColor: SWEET_MAIN_COLOR_BUTTON,
    titleText: "Mercafruver - El Paraiso",
    text: text,
  });
}

function sweetMessageThen(title, icon, text, callback) {
  Swal.fire({
    icon: icon,
    confirmButtonColor: SWEET_MAIN_COLOR_BUTTON,
    titleText: title,
    text: text,
  }).then(() => {
    callback();
  });
}

function sweetExitConfirm(title, text) {
  Swal.fire({
    titleText: title,
    text: text,
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: SWEET_MAIN_COLOR_BUTTON,
    //cancelButtonColor: SWEET_SECONDARY_COLOR_BUTTON,
    confirmButtonText: "Si, salir del sistema",
  }).then((result) => {
    if (result.isConfirmed) {
      //JIO: Ajax para cerrar sesion
      location.href = "/";
    }
  });
}

function getFullDate(fecha) {
  var year = fecha.getFullYear();
  var month = agregarCeros(fecha.getMonth() + 1);
  var day = agregarCeros(fecha.getDate());
  var hour = agregarCeros(fecha.getHours());
  var minute = agregarCeros(fecha.getMinutes());
  var second = agregarCeros(fecha.getSeconds());
  return year + month + day + hour + minute + second;
}

function agregarCeros(valor) {
  if (valor < 10) {
    valor = "0" + valor;
  }
  return valor;
}

function round(number) {
  return Math.round(number * 10) / 10;
}

const DEFAULT_DATATABLE_OPTIONS = {
  responsive: true,
  lengthChange: false,
  autoWidth: false,
  language: {
    info: "Mostrando página _PAGE_ de _PAGES_",
    search: "Buscar:",
    paginate: {
      previous: "Anterior",
      next: "Siguiente",
    },
    buttons: {
      colvis: "Columnas visibles",
      copy: "Copiar",
      copySuccess: {
        1: "Copiada 1 fila al portapapeles",
        _: "Copiadas %d filas al portapapeles",
      },
      copyTitle: "Copiar al portapapeles",
      print: "Imprimir",
    },
  },
  buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
};

function changeInputPassword(inputId) {
  let input = document.getElementById(inputId);
  let label = document.getElementById(inputId + "Label");
  if (input.type === "password") {
    input.type = "text";
    label.classList.remove("fa-eye-slash");
    label.classList.add("fa-eye");
  } else {
    input.type = "password";
    label.classList.remove("fa-eye");
    label.classList.add("fa-eye-slash");
  }
}

function validateInputPassword() {
  var strongRegex = new RegExp(
    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_])(?=.{8,})"
  );
  return strongRegex.test(document.getElementById("password").value);
}

function validateForm(array, exceptions) {
  for (i = 0; i < array.length; i++) {
    if (array[i].name == "password" && array[i].value != "") {
      if (!validateInputPassword()) {
        sweetMessage(
          "warning",
          "La contraseña no cumple con los parámetros establecidos"
        );
        return false;
      }
    }

    if (array[i].value == "" && !exceptions.includes(array[i].name)) {
      sweetMessage("warning", "Por favor, no deje campos vacíos");
      return false;
    }
  }
  return true;
}
