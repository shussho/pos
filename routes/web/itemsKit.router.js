const express = require("express");
const router = express.Router();

router.route("/").get(function (req, res) {
  res.render("adminLTE/dashboard", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

module.exports = router;
