const express = require("express");
const router = express.Router();

router.get("/", function (req, res) {
  res.render("adminLTE/dashboard", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

router.get("/index", function (req, res) {
  res.render("adminLTE/dashboard", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

module.exports = router;
