const express = require("express");
const router = express.Router();
const axios = require("axios");
const UserService = require("../../services/user.service");
const JwtService = require("../../services/jwt.service");

const userService = new UserService();

const jwtService = new JwtService();

router.get("/", (req, res) => {
  res.render("adminLTE/login");
});

// Login de usuario
router.post("/login", function (req, res) {
  axios({
    method: "post",
    url: process.env.DOMAIN + "api/users/login",
    data: {
      email: req.body.email,
      password: req.body.password,
    },
  })
    .then(function (response) {
      if (response.data.success) {
        response.data.redirect = "/web?token=" + response.data.token;
      }
      res.json(response.data);
    })
    .catch(function (error) {
      res.render("message", { success: false, message: error, redirect: "/" });
    });
});

// Rutas para recuperar contraseña
router
  .route("/forgotPassword")
  .get(function (req, res) {
    res.render("adminLTE/forgotPassword");
  })
  .post(function (req, res) {
    const match = {
      email: req.body.email,
    };
    userService.find(match, function (output) {
      if (output.success == true) {
        console.log(output);
        output.redirect = "/";
        output.message =
          "Hemos enviado a tu correo instrucciones para recuperar tu cuenta.";
        const user = output.data[0];
        const token = jwtService.temporaryToken(user);
        // TODO: Probar envío de correos
        const emailBody =
          "Hola " +
          user.firstName +
          ",<br><br>Por medio del siguiente enlace podrás cambiar tu contraseña de usuario.<br><br><a href='" +
          process.env.DOMAIN +
          "changePassword/" +
          user._id +
          "?token=" +
          token +
          "'>Cambiar mi contraseña</a><br><br><br><b>Nota:</b> Este enlace solo tiene validez por 60 minutos.";
        //emailNotification(user.email, 'Recuperar contraseña', emailBody);
      }
      res.json(output);
    });
  });

module.exports = router;
