const express = require("express");
const webRoutes = express.Router();
const axios = require("axios");
const profileHandler = require("../../controllers/profileController");
const commonFunctions = require("../../libraries/commonFunctions");

// Ruta para el manejo de foto de perfil

//-------------------------------------------------------------------------------------------------
webRoutes.route("/profile").post(function (req, res) {
  profileHandler.create(req, function (result) {
    res.send(result);
  });
});
//-------------------------------------------------------------------------------------------------

// **********************************************************************************************************************************

webRoutes.route("/itemKit").get(function (req, res) {
  res.render("web/index", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

// OK

webRoutes.route("/reports").get(function (req, res) {
  res.render("web/index", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

webRoutes.route("/receiving").get(function (req, res) {
  res.render("web/index", {
    success: true,
    message: "Has iniciado sesión correctamente",
    token: req.token,
    user: req.decoded.user,
  });
});

//-------------------------------------------------------------------------------------------------
module.exports = webRoutes;
