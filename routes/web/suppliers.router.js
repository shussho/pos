const express = require("express");
const router = express.Router();
const axios = require("axios");
const commonFunctions = require("../../libraries/commonFunctions");

router.route("/").get(function (req, res) {
  axios({
    method: "get",
    url: process.env.DOMAIN + "api/suppliers",
    headers: { "x-access-token": req.token },
  })
    .then(function (response) {
      if (response.data.success == true) {
        res.render("adminLTE/suppliers/suppliers_list", {
          success: true,
          message: "",
          token: req.token,
          user: req.decoded.user,
          data: response.data.data,
        });
      } else {
        res.render("adminLTE/error", {
          success: false,
          message: error,
          token: req.token,
          user: req.decoded.user,
          error: response.data,
        });
      }
    })
    .catch(function (error) {
      res.render("adminLTE/error", {
        success: false,
        message: error,
        token: req.token,
        user: req.decoded.user,
        error: error,
      });
    });
});

router
  .route("/suppliers")
  .get(function (req, res) {
    res.render("web/createSupplier", {
      success: true,
      message: "",
      token: req.token,
      user: req.decoded.user,
      data: "",
    });
  })
  .post(function (req, res) {
    commonFunctions.axiosGetResult(
      "post",
      "api/suppliers",
      "message",
      req,
      res
    );
  });

router
  .route("/:id")
  .get(function (req, res) {
    axios({
      method: "get",
      url: process.env.DOMAIN + "api/suppliers/" + req.params.id,
      headers: { "x-access-token": req.token },
    })
      .then(function (response) {
        if (response.data.success == true) {
          res.render("web/checkSupplier", {
            success: true,
            message: "",
            token: req.token,
            user: req.decoded.user,
            data: response.data.data[0],
          });
        } else {
          response.data.redirect = "/web/?token=" + req.token;
          res.render("message", response.data);
        }
      })
      .catch(function (error) {
        res.render("message", {
          success: false,
          message: error,
          redirect: "/web/?token=" + req.token,
        });
      });
  })
  .put(function (req, res) {
    axios({
      method: "put",
      url: process.env.DOMAIN + "api/suppliers/" + req.params.id,
      headers: { "x-access-token": req.token },
      data: req.body,
    })
      .then(function (response) {
        if (response.data.success == true) {
          res.render("web/checkSupplier", {
            success: true,
            message: "",
            token: req.token,
            user: req.decoded.user,
            data: response.data.result,
          });
        } else {
          response.data.redirect = "/web/?token=" + req.token;
          res.render("message", response.data);
        }
      })
      .catch(function (error) {
        res.render("message", {
          success: false,
          message: error,
          redirect: "/web/?token=" + req.token,
        });
      });
  });

module.exports = router;
