const express = require("express");
const router = express.Router();
const axios = require("axios");
const commonFunctions = require("../../libraries/commonFunctions");

router.route("/").get(function (req, res) {
  match = req.body;
  axios({
    method: "get",
    url: process.env.DOMAIN + "api/users",
    headers: { "x-access-token": req.token },
  })
    .then(function (response) {
      if (response.data.success == true) {
        res.render("adminLTE/employees/employees_list", {
          success: true,
          message: "",
          token: req.token,
          user: req.decoded.user,
          data: response.data.data,
        });
      } else {
        res.render("adminLTE/error", {
          success: false,
          message: error,
          token: req.token,
          user: req.decoded.user,
          error: response.data,
        });
      }
    })
    .catch(function (error) {
      res.render("adminLTE/error", {
        success: false,
        message: error,
        token: req.token,
        user: req.decoded.user,
        error: error,
      });
    });
});

router
  .route("/user")
  .get(function (req, res) {
    res.render("adminLTE/employees/employees_create", {
      success: true,
      message: "",
      token: req.token,
      user: req.decoded.user,
      data: "",
    });
  })
  .post(function (req, res) {
    commonFunctions.axiosGetResult(
      "post",
      "api/users",
      req,
      res,
      function (retorno) {
        console.log(retorno);
        res.json(retorno);
      }
    );
  });

router
  .route("/user/:id")
  .get(function (req, res) {
    axios({
      method: "get",
      url: process.env.DOMAIN + "api/users/" + req.params.id,
      headers: { "x-access-token": req.token },
    })
      .then(function (response) {
        if (response.data.success == true) {
          res.render("adminLTE/employees/employees_" + req.query.type, {
            // El req.query.type define el tipo de template que debe mostrarse al usuario
            success: true,
            message: "",
            token: req.token,
            user: req.decoded.user,
            data: response.data.data[0],
          });
        } else {
          res.render("adminLTE/error", {
            success: false,
            message: error,
            token: req.token,
            user: req.decoded.user,
            error: response.data,
          });
        }
      })
      .catch(function (error) {
        res.render("adminLTE/error", {
          success: false,
          message: error,
          token: req.token,
          user: req.decoded.user,
          error: error,
        });
      });
  })
  .put(function (req, res) {
    commonFunctions.axiosGetResult(
      "put",
      "api/users/" + req.params.id,
      req,
      res,
      function (retorno) {
        res.json(retorno);
      }
    );
  });

router.route("/changePicture/:id").put(function (req, res) {
  commonFunctions.axiosGetResult(
    "put",
    "api/users/changePicture/" + req.params.id,
    req,
    res,
    function (retorno) {
      res.json(retorno);
    }
  );
});

module.exports = router;
