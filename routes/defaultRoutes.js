const express = require("express");
const router = express.Router();
const defaultRouter = require("./default/default.router");

function routerDefault(app) {
  app.use("/", router);
  router.use("/", defaultRouter);
}

module.exports = routerDefault;
