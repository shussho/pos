const express = require("express");
const router = express.Router();
const usersRouter = require("./api/users.router");
const itemsRouter = require("./api/items.router");
const suppliersRouter = require("./api/suppliers.router");
const apiMiddleware = require("../middleware/apiMiddleware");

function routerApi(app) {
  app.use("/api", apiMiddleware);
  app.use("/api", router);
  router.use("/users", usersRouter);
  router.use("/items", itemsRouter);
  router.use("/suppliers", suppliersRouter);
}

module.exports = routerApi;
