const express = require("express");
const mongoose = require("mongoose"); // Conector de la DB
const router = express.Router();
const ItemService = require("../../services/item.service");
const service = new ItemService();

router
  .route("/")
  .get((req, res) => {
    let match = req.query;
    service.find(match, (result) => res.json(result));
  })
  .post((req, res) => {
    service.create(req, (result) => res.json(result));
  });

router
  .route("/:id")
  .get((req, res) => {
    let match = { _id: mongoose.Types.ObjectId(req.params.id) };
    service.find(match, (result) => res.json(result));
  })
  .put((req, res) => {
    service.update(req, (result) => res.json(result));
  })
  .delete((req, res) => {
    service.delete(req, (result) => res.json(result));
  });

module.exports = router;
