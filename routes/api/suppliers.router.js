const express = require("express");
const router = express.Router();
const SupplierService = require("../../services/supplier.service");
const service = new SupplierService();

router
  .route("/")
  .get(function (req, res) {
    match = req.query;
    service.find(match, (result) => res.json(result));
  })
  .post(function (req, res) {
    service.create(req, (result) => res.json(result));
  });

router
  .route("/:id")
  .get(function (req, res) {
    let match = { _id: req.params.id };
    service.find(match, (result) => res.json(result));
  })
  .put(function (req, res) {
    service.update(req, (result) => res.json(result));
  })
  .delete(function (req, res) {
    service.delete(req, (result) => res.json(result));
  });

module.exports = router;
