const express = require("express");
const mongoose = require("mongoose"); // Conector de la DB
const router = express.Router();
const UserService = require("../../services/user.service");

const userService = new UserService();

router.route("/login").post(function (req, res) {
  userService.login(req, function (result) {
    res.json(result);
  });
});

router.route("/changePicture/:id").put(function (req, res) {
  userService.updatePicture(req, function (output) {
    res.json(output);
  });
});

router
  .route("/")
  .get(function (req, res) {
    match = req.body;
    userService.find(match, function (result) {
      res.json(result);
    });
  })
  .post(function (req, res) {
    // Crear nuevo usuario
    userService.create(req, function (result) {
      res.json(result);
    });
  });

router
  .route("/:id")
  .get(function (req, res) {
    match = { _id: mongoose.Types.ObjectId(req.params.id) };
    userService.find(match, function (result) {
      res.json(result);
    });
  })
  .put(function (req, res) {
    if (req.body.password == "") {
      delete req.body.password;
    }
    userService.update(req, function (result) {
      res.json(result);
    });
  })
  .delete(function (req, res) {
    userService.delete(req, function (result) {
      res.json(result);
    });
  });

module.exports = router;
