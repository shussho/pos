const express = require("express");
const router = express.Router();
const defaultRouter = require("./web/default.router");
const deliveriesRouter = require("./web/deliveries.router");
const employeesRouter = require("./web/employees.router");
const indexRouter = require("./web/index.router");
const itemsRouter = require("./web/items.router");
const itemsKitRouter = require("./web/itemsKit.router");
const receiveMerchandiseRouter = require("./web/receiveMerchandise.router");
const reportsRouter = require("./web/reports.router");
const salesRouter = require("./web/sales.router");
const settingsRouter = require("./web/settings.router");
const suppliersRouter = require("./web/suppliers.router");
const webMiddleware = require("../middleware/webMiddleware");

function routerWeb(app) {
  app.use("/", router);
  router.use("/", defaultRouter);

  app.use("/web", webMiddleware);
  app.use("/web", router);

  router.use("/", indexRouter);
  router.use("/deliveries", deliveriesRouter);
  router.use("/employees", employeesRouter);
  router.use("/items", itemsRouter);
  router.use("/itemsKit", itemsKitRouter);
  router.use("/receiveMerchandise", receiveMerchandiseRouter);
  router.use("/reports", reportsRouter);
  router.use("/sales", salesRouter);
  router.use("/settings", settingsRouter);
  router.use("/suppliers", suppliersRouter);
}

module.exports = routerWeb;
