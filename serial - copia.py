#! /usr/bin/env python
# encoding: utf-8



import sys
import time
#sys.path.insert(0, '..')
import serial
#from serial import SerialException

#import serial.tools.list_ports
#print([comport.device for comport in serial.tools.list_ports.comports()])



#Valida si el puerto esta disponible
def portIsUsable(port):
    try:
       port.open()
       return True
    except:
       return False





# Se definen los valores para la conexión serial
ser = serial.Serial()
ser.baudrate = 115200
ser.port = 'COM3'
#ser.port = sys.argv[1] ->> Si se pasa como argumento desde CLI
ser.timeout = 5

# Se realiza validación del puerto
if not portIsUsable(ser): 
    print("El puerto está en uso o no disponible")
else:
    #print(ser.name)     # check which port was really used
    ser.write(b'P')     # write a string
    #ser.write(bytes(b'P'))

    #Se debe especificar el tamaño del texto recibido (PCR devuelve 11 bytes)
    s = ser.read(11)
    s = str(s,'utf-8')
    #s = s.replace('\r', '')
    print(s.strip())
    ser.close()             # close port